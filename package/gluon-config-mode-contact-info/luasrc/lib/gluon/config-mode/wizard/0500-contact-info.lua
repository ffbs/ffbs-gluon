return function(form, uci)
	local pkg_i18n = i18n 'gluon-config-mode-contact-info'
	local site_i18n = i18n 'gluon-site'

	local help = site_i18n._translate("gluon-config-mode:contact-help") or pkg_i18n.translate(
		'Please provide your contact information here to allow others to contact '
		.. 'you. Note that this information will be visible <em>publicly</em> on '
		.. 'the internet together with your node\'s coordinates. This means it can '
		.. 'be downloaded and processed by anyone. This information is '
		.. 'not required to operate a node. If you chose to enter data, it will be '
		.. 'stored on this node and can be deleted by yourself at any time.'
	)
	local s = form:section(Section, nil, help)

	local publish = s:option(Flag, "publish", translate("Publish contact info on node-info page"))
	publish.default=uci:get_bool("parker","owner","publish")
	function publish:write(data)
		uci:section("parker", "owner", "owner")
		uci:set("parker", "owner", "publish", data)
		local owner = uci:get_first("gluon-node-info", "owner")
		if data then
			local contact = uci:get("parker", "owner", "contact")
			uci:set("gluon-node-info", owner, "contact", contact)
		else
			uci:set("gluon-node-info", owner, "contact", "")
		end
	end

	local o = s:option(Value, "contact", pkg_i18n.translate("Contact info"),
		site_i18n._translate("gluon-config-mode:contact-note") or pkg_i18n.translate("e.g. E-mail or phone number"))
	o.default = uci:get("parker", "owner", "contact")
	o.datatype = 'minlength(1)'
	o.optional = true
	function o:write(data)
		uci:section("parker","owner","owner")
		uci:set("parker", "owner", "contact", data)

		local owner = uci:get_first("gluon-node-info", "owner")
		local publish = uci:get_bool("parker", "owner", "publish")
		if publish then
			uci:set("gluon-node-info", owner, "contact", data)
		else
			uci:set("gluon-node-info", owner, "contact", "")
		end

		uci:save("parker")
		uci:save("gluon-node-info")
	end
	return {'parker'}
end
